Source: python-octaviaclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper (>= 9),
 dh-python,
 openstack-pkg-tools,
 python-all,
 python-pbr (>= 2.0.0),
 python-setuptools,
 python3-all,
 python3-pbr (>= 2.0.0),
 python3-setuptools,
 python3-sphinx (>= 1.6.2),
Build-Depends-Indep:
 python-appdirs,
 python-babel,
 python-cliff (>= 2.6.0),
 python-cmd2,
 python-coverage,
 python-debtcollector,
 python-funcsigs,
 python-hacking,
 python-iso8601,
 python-keystoneauth1 (>= 3.4.0),
 python-mock,
 python-monotonic,
 python-netaddr,
 python-netifaces,
 python-neutronclient (>= 1:6.7.0),
 python-openstackclient (>= 3.12.0),
 python-os-client-config (>= 1.28.0),
 python-osc-lib (>= 1.11.0),
 python-oslo.i18n (>= 3.15.3),
 python-oslo.serialization,
 python-oslo.utils (>= 3.33.0),
 python-oslotest (>= 3.2.0),
 python-prettytable (>= 0.7.1),
 python-pyparsing (>= 2.1.0),
 python-requests-mock (>= 1.1),
 python-requestsexceptions (>= 1.2.0),
 python-simplejson,
 python-six,
 python-stestr (>= 2.0.0),
 python-stevedore (>= 1.20.0),
 python-testscenarios,
 python-tz,
 python-unicodecsv,
 python-wrapt,
 python-yaml,
 python3-appdirs,
 python3-babel,
 python3-cliff (>= 2.6.0),
 python3-cmd2,
 python3-debtcollector,
 python3-funcsigs,
 python3-iso8601,
 python3-keystoneauth1 (>= 3.4.0),
 python3-mock,
 python3-monotonic,
 python3-netaddr,
 python3-netifaces,
 python3-neutronclient (>= 1:6.7.0),
 python3-openstackclient (>= 3.12.0),
 python3-openstackdocstheme (>= 1.18.1),
 python3-os-client-config (>= 1.28.0),
 python3-osc-lib (>= 1.11.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.serialization,
 python3-oslo.utils (>= 3.33.0),
 python3-oslotest (>= 3.2.0),
 python3-prettytable (>= 0.7.1),
 python3-pyparsing (>= 2.1.0),
 python3-requests-mock (>= 1.1),
 python3-requestsexceptions (>= 1.2.0),
 python3-simplejson,
 python3-six,
 python3-stestr (>= 2.0.0),
 python3-stevedore (>= 1.20.0),
 python3-testscenarios,
 python3-tz,
 python3-wrapt,
 python3-yaml,
 subunit,
 testrepository,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-octaviaclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-octaviaclient.git
Homepage: http://docs.openstack.org/developer/python-octaviaclient

Package: python-octaviaclient
Architecture: all
Depends:
 python-appdirs,
 python-babel,
 python-cliff (>= 2.6.0),
 python-cmd2,
 python-debtcollector,
 python-funcsigs,
 python-iso8601,
 python-keystoneauth1 (>= 3.4.0),
 python-monotonic,
 python-netaddr,
 python-netifaces,
 python-neutronclient (>= 1:6.7.0),
 python-openstackclient (>= 3.12.0),
 python-os-client-config (>= 1.28.0),
 python-osc-lib (>= 1.11.0),
 python-oslo.i18n (>= 3.15.3),
 python-oslo.serialization,
 python-oslo.utils (>= 3.33.0),
 python-pbr (>= 2.0.0),
 python-prettytable (>= 0.7.1),
 python-pyparsing (>= 2.1.0),
 python-requests (>= 2.14.2),
 python-requestsexceptions (>= 1.2.0),
 python-simplejson,
 python-six,
 python-stevedore (>= 1.20.0),
 python-tz,
 python-unicodecsv,
 python-wrapt,
 python-yaml,
 ${misc:Depends},
 ${python:Depends},
Suggests:
 python-octaviaclient-doc,
Description: octavia client for OpenStack Load Balancing - Python 2.7
 Octavia is an operator-grade open source scalable load balancer for use in
 large OpenStack deployments. It delivers load balancing services on amphorae
 and provides centralized command and control. Octavia is currently the
 reference backend for Neutron LBaaS. In the near future, Octavia is likely to
 become the standard OpenStack LBaaS API endpoint.
 .
 This package contains the Python 2.7 module.

Package: python-octaviaclient-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Octavia client for OpenStack Load Balancing - doc
 Octavia is an operator-grade open source scalable load balancer for use in
 large OpenStack deployments. It delivers load balancing services on amphorae
 and provides centralized command and control. Octavia is currently the
 reference backend for Neutron LBaaS. In the near future, Octavia is likely to
 become the standard OpenStack LBaaS API endpoint.
 .
 This package contains the documentation.

Package: python3-octaviaclient
Architecture: all
Depends:
 python3-appdirs,
 python3-babel,
 python3-cliff (>= 2.6.0),
 python3-cmd2,
 python3-debtcollector,
 python3-funcsigs,
 python3-iso8601,
 python3-keystoneauth1 (>= 3.4.0),
 python3-monotonic,
 python3-netaddr,
 python3-netifaces,
 python3-neutronclient (>= 1:6.7.0),
 python3-openstackclient (>= 3.12.0),
 python3-os-client-config (>= 1.28.0),
 python3-osc-lib (>= 1.11.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.serialization,
 python3-oslo.utils (>= 3.33.0),
 python3-pbr (>= 2.0.0),
 python3-prettytable (>= 0.7.1),
 python3-pyparsing (>= 2.1.0),
 python3-requests (>= 2.14.2),
 python3-requestsexceptions (>= 1.2.0),
 python3-simplejson,
 python3-six,
 python3-stevedore (>= 1.20.0),
 python3-tz,
 python3-wrapt,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-octaviaclient-doc,
Description: Octavia client for OpenStack Load Balancing - Python 3.x
 Octavia is an operator-grade open source scalable load balancer for use in
 large OpenStack deployments. It delivers load balancing services on amphorae
 and provides centralized command and control. Octavia is currently the
 reference backend for Neutron LBaaS. In the near future, Octavia is likely to
 become the standard OpenStack LBaaS API endpoint.
 .
 This package contains the Python 3.x module.
